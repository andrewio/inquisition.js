const { Inquisitor } = require('../index')

class Second extends Inquisitor {
  async testFail() {
    this.assert(1 === 2)
  }

  async testPass() {
    this.assertEqual(1, 1)
  }

  async testAsync() {
    return new Promise(() => {})
  }

  async testError() {
    throw new Error('Boom!')
  }
}

module.exports = Second
