const fs = require('fs')
const path = require('path')

class TestFinder {
  constructor({ startDirectory }) {
    this.directoriesToSearch = [startDirectory]
    this.testFiles = []
  }

  find() {
    return new Promise(this._findPromiseHandler.bind(this))
  }

  _findPromiseHandler(resolve, reject) {
    this._walk(resolve, reject, this.directoriesToSearch.pop())
  }

  _walk(resolve, reject, currentDirectory) {
    fs.readdir(
      currentDirectory,
      {
        encoding: 'utf8',
        withFileTypes: true,
      },
      this._handleReadDir(resolve, reject, currentDirectory).bind(this),
    )
  }

  _handleReadDir(resolve, reject, currentDirectory) {
    return (err, files) => {
      if (err) {
        reject(err)
      } else {
        files.forEach(file => {
          const currentFile = path.join(currentDirectory, file.name)
          if (file.isFile() && file.name.match(/\.trial\.js$/)) {
            this.testFiles.push(currentFile)
          } else if (file.isDirectory()) {
            this.directoriesToSearch.push(currentFile)
          }
        })
      }

      if (this.directoriesToSearch.length > 0) {
        this._walk(resolve, reject, this.directoriesToSearch.pop())
      } else {
        resolve(this.testFiles)
      }
    }
  }
}

module.exports = TestFinder
