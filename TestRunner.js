#!/usr/bin/env node
const TestFinder = require('./TestFinder')
const Reporter = require('./lib/Reporter')
const Util = require('./lib/Util')

async function runTests() {
  const ROOT_DIR = process.cwd()
  const classes = new Map()
  const reporter = new Reporter()
  const testFinder = new TestFinder({ startDirectory: ROOT_DIR })

  reporter.header()

  try {
    const testFiles = await testFinder.find()
    const start = Date.now()
    Util.shuffleArray(testFiles).forEach(testFile =>
      classes.set(testFile, require(testFile)),
    )

    let testSuites = []
    classes.forEach((importedFile, filePath) =>
      testSuites.push(importedFile.run({ reporter, path: filePath })),
    )

    await Promise.all(testSuites)
    const finish = Date.now()
    console.log('\n')
    reporter.timingSummary({ start, finish })
    reporter.summary()
    reporter.statisticSummary()
  } catch (e) {
    console.log(e)
  }
}

runTests()
