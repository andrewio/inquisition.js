const { Inquisitor } = require('./index')

class First extends Inquisitor {
  async testFail() {
    this.assert(1 === 2)
  }

  async testPass() {
    const x = 1
    this.assert(x === 1)
  }

  async testSkip() {
    this.skip()
    console.log('woot')
  }
}

module.exports = First
