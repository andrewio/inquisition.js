const Colors = require('./Colors')

class AbbreviatedStackSkipReport {
  constructor({ index, result }) {
    this.index = index
    this.result = result
  }

  stack() {
    const re = new RegExp(
      `\\(${this.result.path.replace('/', '/').replace(/\./g, '\\.')}.*\\)`,
    )
    return this.result.failure.stack.match(re) || ''
  }

  report() {
    console.log(`${Colors.FG_CYAN}${this.index}) Skip:`)
    console.log(
      `${this.result.constructor.name}#${this.result.name} ${this.stack()}`,
    )
    console.log(this.result.failure.message)
    console.log(Colors.RESET)
  }
}

module.exports = AbbreviatedStackSkipReport
