class AssertionError extends Error {
  constructor({ message, expected, actual }) {
    super(message)
    this.name = this.constructor.name
    this.expected = expected
    this.actual = actual
  }
}

module.exports = AssertionError
