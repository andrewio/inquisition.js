class SkipError extends Error {
  constructor({ message = 'No reason given' }) {
    super(message)
    this.name = this.constructor.name
  }
}

module.exports = SkipError
