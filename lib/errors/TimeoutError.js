class TimeoutError extends Error {
  constructor({ timeout }) {
    super(`Awaited test timed out after ${timeout}ms`)
    this.name = this.constructor.name
  }
}

module.exports = TimeoutError
