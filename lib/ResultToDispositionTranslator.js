const AssertionError = require('./errors/AssertionError')
const SkipError = require('./errors/SkipError')

class ResultToDispositionTranslator {
  constructor(result) {
    this.failure = result.failure
  }

  toString() {
    if (this.failure instanceof AssertionError) {
      return 'Failure'
    } else if (this.failure instanceof SkipError) {
      return 'Skip'
    } else {
      return 'Error'
    }
  }
}

module.exports = ResultToDispositionTranslator
