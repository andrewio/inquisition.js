const Colors = require('./Colors')

class AbbreviatedStackFailureReport {
  constructor({ index, result }) {
    this.index = index
    this.result = result
  }

  stack() {
    const re = new RegExp(
      `\\(${this.result.path.replace('/', '/').replace(/\./g, '\\.')}.*\\)`,
    )
    return this.result.failure.stack.match(re) || ''
  }

  report() {
    console.log(`${Colors.FG_RED}${this.index}) Failure:`)
    console.log(
      `${this.result.constructor.name}#${this.result.name} ${this.stack()}`,
    )
    console.log(`Expected: ${this.result.failure.expected}`)
    console.log(`  Actual: ${this.result.failure.actual}`)
    console.log(Colors.RESET)
  }
}

module.exports = AbbreviatedStackFailureReport
