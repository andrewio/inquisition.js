const TimeoutError = require('./errors/TimeoutError')

class AsyncRunner {
  constructor({ func, timeout, path }) {
    this.func = func
    this.timeout = timeout
    this.setTimeoutId = null
    this.path = path
  }

  run() {
    return new Promise(this.promiseHandler.bind(this))
  }

  async promiseHandler(resolve, reject) {
    try {
      this.setTimeoutId = setTimeout(
        this.timeoutHandler.bind(this, reject),
        this.timeout,
      )
      const result = await this.func()
      resolve(result)
    } catch (e) {
      reject(e)
    } finally {
      clearTimeout(this.setTimeoutId)
    }
  }

  timeoutHandler(reject) {
    const error = new TimeoutError({ timeout: this.timeout })
    error.stack = `(${this.path})`
    reject(error)
  }
}

module.exports = AsyncRunner
