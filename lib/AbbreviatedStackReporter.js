const AbbreviatedStackReportFactory = require('./AbbreviatedStackReportFactory')

class AbbreviatedStackReporter {
  constructor({ index, disposition, result }) {
    this.index = index
    this.disposition = disposition
    this.result = result
  }

  report() {
    const init = { index: this.index, result: this.result }
    AbbreviatedStackReportFactory.for({
      disposition: this.disposition,
      init,
    }).report()
  }
}

module.exports = AbbreviatedStackReporter
