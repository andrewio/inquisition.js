// For now this is only used for timeout errors
// If it needs to be used for other errors in the future
// a proper stack trace will need to be derived
const Colors = require('./Colors')

class AbbreviatedStackErrorReport {
  constructor({ index, result }) {
    this.index = index
    this.result = result
  }

  report() {
    console.log(`${Colors.FG_RED}${this.index}) Error:`)
    console.log(
      `${this.result.constructor.name}#${this.result.name} (${
        this.result.path
      })`,
    )
    console.log(`${this.result.failure.message}`)
    console.log(Colors.RESET)
  }
}

module.exports = AbbreviatedStackErrorReport
