const ResultToShortCodeTranslator = require('./ResultToShortCodeTranslator')
const ResultToDispositionTranslator = require('./ResultToDispositionTranslator')
const ReporterFactory = require('./ReporterFactory')
const SkipError = require('./errors/SkipError')
const AssertionError = require('./errors/AssertionError')

class Reporter {
  constructor() {
    this.failures = []
    this.runCount = 0
    this.assertionCount = 0
    this.failureCount = 0
    this.skipCount = 0
    this.errorCount = 0
  }

  async push(resultPromise) {
    const result = await resultPromise
    this.runCount++
    this.assertionCount += result.assertions
    if (result.failure) {
      this.failures.push(result)
      this.incrementFailureCount(result.failure)
    }
    process.stdout.write(new ResultToShortCodeTranslator(result).toString())
  }

  incrementFailureCount(failure) {
    if (failure instanceof SkipError) {
      this.skipCount++
    } else if (failure instanceof AssertionError) {
      this.failureCount++
    } else {
      this.errorCount++
    }
  }

  header() {
    console.log('Running:')
    console.log()
  }

  timingSummary({ start, finish }) {
    const duration = (finish - start) / 1000
    const runsASecond = 1 / (duration / this.runCount)
    const assertionsASecond = 1 / (duration / this.assertionCount)
    const localeParams = {
      minimumFractionDigits: 2,
      maximimumFractionDigits: 2,
    }
    console.log(
      `Finished in ${duration.toLocaleString(
        'en-US',
        localeParams,
      )}s, ${runsASecond.toLocaleString(
        'en-US',
        localeParams,
      )} runs/s, ${assertionsASecond.toLocaleString(
        'en-US',
        localeParams,
      )} assertions/s.`,
    )
    console.log()
  }

  statisticSummary() {
    console.log(
      `${this.runCount} runs, ${this.assertionCount} assertions, ${
        this.failureCount
      } failures, ${this.skipCount} skips, ${this.errorCount} errors`,
    )
  }

  summary() {
    this.failures.forEach((result, i) => {
      const index = i + 1
      const disposition = new ResultToDispositionTranslator(result).toString()
      const ReporterKlass = ReporterFactory.for(result.failure)
      new ReporterKlass({ index, disposition, result }).report()
    })
  }
}

module.exports = Reporter
