/* eslint class-methods-use-this: 0 */
/* eslint no-empty-function: 0 */

const util = require('util')
const AsyncRunner = require('./AsyncRunner')
const AssertionError = require('./errors/AssertionError')
const TimeoutError = require('./errors/TimeoutError')
const SkipError = require('./errors/SkipError')
const Util = require('./Util')

class Inquisitor {
  static async run({ reporter, path }) {
    const tests = []
    const testNames = this.testNames()
    testNames.forEach(name => {
      const deferredTest = new this({ name, path }).run()
      reporter.push(deferredTest)
      tests.push(deferredTest)
    })

    await Promise.all(tests)
  }

  static testNames() {
    return Util.shuffleArray(
      Object.getOwnPropertyNames(this.prototype).filter(instanceMethod =>
        instanceMethod.match(/^test/),
      ),
    )
  }

  constructor({ name, path }) {
    this.name = name
    this.failure = false
    this.timeout = this.constructor.timeout
    this.path = path
    this.assertions = 0
  }

  async setup() {}

  async teardown() {}

  _assert({ test, message, actual, expected }) {
    this.assertions += 1
    if (test !== true) {
      throw new AssertionError({ message, expected, actual })
    }
  }

  assert(test) {
    this._assert({
      test,
      message: 'Expected true',
      actual: false,
      expected: true,
    })
  }

  refute(test) {
    this._assert({
      test,
      message: 'Expected false',
      actual: true,
      expected: false,
    })
  }

  assertEqual({ actual, expected }) {
    this._assert({
      test: util.isDeepStrictEqual(actual, expected),
      message: 'Expected values to equal',
      actual,
      expected,
    })
  }

  refuteEqual({ actual, expected }) {
    this._assert({
      test: !util.isDeepStrictEqual(actual, expected),
      message: 'Expected values to equal',
      actual,
      expected,
    })
  }

  assertInDelta({ actual, expected, delta = 0.001 }) {
    this._assert({
      test: Math.abs(actual - expected) < delta,
      message: `Expected values to be within ${delta} of each other`,
      actual,
      expected,
    })
  }

  refuteInDelta({ actual, expected, delta = 0.001 }) {
    this._assert({
      test: Math.abs(actual - expected) > delta,
      message: `Expected values not to be within ${delta} of each other`,
      actual,
      expected,
    })
  }

  skip(message = 'No reason given') {
    throw new SkipError(message)
  }

  async run() {
    try {
      await new AsyncRunner({
        func: this.setup.bind(this),
        timeout: this.timeout,
        path: this.path,
      }).run()
    } catch (e) {
      if (e instanceof TimeoutError) {
        this.name = 'setup'
      }
      this.failure = e
      return this
    }

    try {
      await new AsyncRunner({
        func: this[this.name].bind(this),
        timeout: this.timeout,
        path: this.path,
      }).run()
    } catch (e) {
      this.failure = e
      return this
    }

    try {
      await new AsyncRunner({
        func: this.teardown.bind(this),
        timeout: this.timeout,
        path: this.path,
      }).run()
    } catch (e) {
      if (e instanceof TimeoutError) {
        this.name = e.methodName
      }
      this.failure = e
      return this
    }

    return this
  }
}

Inquisitor.timeout = 2000

module.exports = Inquisitor
