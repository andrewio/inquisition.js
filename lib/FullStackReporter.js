const Colors = require('./Colors')

class FullStackReporter {
  constructor({ index, disposition, result }) {
    this.index = index
    this.disposition = disposition
    this.result = result
  }

  report() {
    console.log(`${Colors.FG_RED}${this.index}) ${this.disposition}:`)
    console.log(`${this.result.constructor.name}#${this.result.name}`)
    console.log(`${this.result.failure.stack}`)
    console.log(Colors.RESET)
  }
}

module.exports = FullStackReporter
