const AbbreviatedStackReporter = require('./AbbreviatedStackReporter')
const FullStackReporter = require('./FullStackReporter')
const AssertionError = require('./errors/AssertionError')
const SkipError = require('./errors/SkipError')
const TimeoutError = require('./errors/TimeoutError')

class ReporterFactory {
  static for(e) {
    if (
      e instanceof AssertionError ||
      e instanceof SkipError ||
      e instanceof TimeoutError
    ) {
      return AbbreviatedStackReporter
    } else {
      return FullStackReporter
    }
  }
}

module.exports = ReporterFactory
