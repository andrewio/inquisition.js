const AssertionError = require('./errors/AssertionError')
const SkipError = require('./errors/SkipError')

class ResultToShortCodeTranslator {
  constructor(result) {
    this.failure = result.failure
  }

  toString() {
    if (!this.failure) return '.'

    if (this.failure instanceof AssertionError) {
      return 'F'
    } else if (this.failure instanceof SkipError) {
      return 'S'
    } else {
      return 'E'
    }
  }
}

module.exports = ResultToShortCodeTranslator
