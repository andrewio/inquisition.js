const AbbreviatedStackSkipReport = require('./AbbreviatedStackSkipReport')
const AbbreviatedStackFailureReport = require('./AbbreviatedStackFailureReport')
const AbbreviatedStackErrorReport = require('./AbbreviatedStackErrorReport')

class AbbreviatedStackReportFactory {
  static for({ disposition, init }) {
    switch (disposition) {
      case 'Skip':
        return new AbbreviatedStackSkipReport(init)
      case 'Failure':
        return new AbbreviatedStackFailureReport(init)
      case 'Error':
        return new AbbreviatedStackErrorReport(init)
      default:
        throw new Error('Unexpected disposition')
    }
  }
}

module.exports = AbbreviatedStackReportFactory
